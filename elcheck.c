#include <errno.h>
#include <stdio.h>
#include <sys/select.h>
#include <sys/un.h>
#include <unistd.h>

#include "electionlib.h"

#define MAX_ERRORS 5
#define REQUIRED_SUCCESSES 10


static int parse_args(int argc, char** argv, el_conf_t* out) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <bcast_address> <client_id>\n", argv[0]);
        return EXIT_ARGS;
    }

    int retval = 0;
    if (el_parse_arg_bcast_address(argv[1], &out->bcast_address))
        retval = EXIT_ARGS;
    if (el_parse_arg_client_id(argv[2], &out->client_id))
        retval = EXIT_ARGS;
    return retval;
}


// ENTRY POINT /////////////////////////////////////////////////////////////////////////////////////////////////////////

/// Program entry point - prepares config, socket, pinger thread; then calls the main peer logic.
int main(int argc, char** argv) {
    el_conf_t conf;
    if (parse_args(argc, argv, &conf)) {
        return EXIT_ARGS;
    }

    printf("Broadcast address:  %s\n"
           "Client ID to check: %lu\n",
           inet_ntoa(conf.bcast_address), conf.client_id);
    fflush(stdout);

    int retval = 0;
    int sockid;

    if ((sockid = socket(AF_UNIX, SOCK_DGRAM, 0)) < 0) {
        perror("Could not create healthcheck socket");
        return EXIT_SYS;
    }

    struct sockaddr_un saddr_chk = {
        .sun_family = AF_UNIX
    };
    strncpy(saddr_chk.sun_path, EL_SOCK_CHK_NAME, sizeof(saddr_chk.sun_path));

    unlink(saddr_chk.sun_path);
    if (bind(sockid, (struct sockaddr*) &saddr_chk, sizeof(saddr_chk)) < 0) {
        perror("Failed to bind");
        retval = EXIT_SYS;
        goto exit_sock;
    }

    size_t errors = 0;
    size_t successes = 0;

    char msg_buffer[512];
    el_msg_t* msg = (el_msg_t*) msg_buffer;
    fd_set fds;

    for (;;) { // "forever" ^_^
        if (successes >= REQUIRED_SUCCESSES) {
            puts("Healthcheck successful.");
            break;
        }
        if (errors >= MAX_ERRORS) {
            fputs("Maximum num. of errors exceeded. Healthcheck failed.\n", stderr);
            retval = EXIT_HEALTHCHECK;
            break;
        }

        FD_ZERO(&fds);
        FD_SET(sockid, &fds);
        struct timespec ts = { .tv_nsec = 500 * 1000000 };
        int sel = pselect(sockid + 1, &fds, NULL, NULL, &ts, NULL);
        if (sel < 0) {
            errors++;
            perror("pselect");
            continue;
        }
        if (!FD_ISSET(sockid, &fds)) {
            errors++;
            fputs("timed out...\n", stderr);
            continue;
        }

        ssize_t received = recvfrom(sockid, msg_buffer, sizeof(msg_buffer), 0, NULL, NULL);

        if (received < 0) {
            perror("recvfrom failed");
            errors++;
            break;
        }

        if (received != ntohs(msg->length)) {
            fputs("Incorrect received message size\n", stderr);
            errors++;
            continue;
        }

        if (msg->magic != PROTOCOL_MAGIC) {
            fputs("WTF!\n", stderr);
            errors++;
            continue;
        }

        if (ntohs(msg->type) != EL_MSG_HELLO) {
            fputs("Non-HELLO message in socket\n", stderr);
            errors++;
            continue;
        }

        uint32_t sender_id = ntohl(msg->client_id);
        if (sender_id != conf.client_id) {
            fputs("Incorrect client id\n", stderr);
            errors++;
            continue;
        }

        puts("Got valid HELLO!");
        successes++;
    }

    puts("Cave Johnson. We're done here.");

exit_sock:
    close(sockid);
    unlink(saddr_chk.sun_path);
    return retval;
}

