#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "electionlib.h"


// CLI CONFIGURATION ///////////////////////////////////////////////////////////////////////////////////////////////////

int el_parse_arg_bcast_address(const char* in, struct in_addr* out) {
    if (!inet_aton(in, out)) {
        fprintf(stderr, "Could not parse bcast_address: %s\n", in);
        return EXIT_ARGS;
    }
    return EXIT_SUCCESS;
}

int el_parse_arg_client_id(const char* in, long* out) {
    char* ciend = NULL;
    *out = strtol(in, NULL, 10);
    if (errno || (ciend && *ciend)) {
        fprintf(stderr, "Could not parse client_id (%s): %s\n", strerror(errno), in);
        return EXIT_ARGS;
    }
    return EXIT_SUCCESS;
}

struct timespec get_waituntil(long long reltime) {
    struct timespec waituntil = {};
    if (clock_gettime(CLOCK_MONOTONIC, &waituntil)) {
        perror("clock_gettime");
        return waituntil;
    }
    waituntil.tv_nsec += reltime * 1000000;
    waituntil.tv_sec += waituntil.tv_nsec / 1000000000;
    waituntil.tv_nsec %= 1000000000;
    return waituntil;
}

