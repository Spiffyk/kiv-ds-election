FROM alpine:3

ENV BCAST_ADDRESS=""
ENV CLIENT_ID=""

COPY ./election ./elcheck ./

HEALTHCHECK --interval=5s \
    CMD ./elcheck "$BCAST_ADDRESS" "$CLIENT_ID" || exit 1

ENTRYPOINT ./election "$BCAST_ADDRESS" "$CLIENT_ID"

