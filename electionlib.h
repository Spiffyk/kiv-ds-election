#ifndef LIBELECTION_H // Header guard
#define LIBELECTION_H

#include <arpa/inet.h>
#include <stdint.h>
#include <time.h>


// CONSTANTS ///////////////////////////////////////////////////////////////////////////////////////////////////////////

#define EXIT_ARGS 1
#define EXIT_SYS 2
#define EXIT_HEALTHCHECK 3

#define EL_PORT 1337  ///< The default UDP port on which the program operates
#define EL_SOCK_CHK_NAME "election.sock"  ///< The filename of the local socket for health checking


// PROTOCOL STUFF //////////////////////////////////////////////////////////////////////////////////////////////////////

#define PROTOCOL_MAGIC htonl(0xCAFFEFEE)

/// The message type code
enum el_msg_type {
    EL_MSG_NULL = 0x0000,
    EL_MSG_HELLO = 0x0001,
    EL_MSG_SETCOLOR = 0x0002,
    EL_MSG_SET_PING = 0x0101,
    EL_MSG_STOP = 0xFFFF
};

/// The base of a protocol message
typedef struct el_msg {
    uint32_t magic;        ///< Always `PROTOCOL_MAGIC`
    int32_t client_id;     ///< Client's identifier (from program argument)
    uint16_t type;         ///< Message type
    uint16_t length;       ///< Length of the full message (including this header)
    uint32_t _reserved;    ///< Unused
} el_msg_t;

/// Makes the receiving peer change their color
typedef struct el_msg_color {
    el_msg_t header;
    uint8_t color;         ///< Color identifier as defined by `el_color` enum
} el_msg_color_t;

/// Makes the peer enable or disable their `HELLO` messages
typedef struct el_msg_set_ping {
    el_msg_t header;
    uint8_t enable;
} el_msg_set_ping_t;


// CLI CONFIGURATION ///////////////////////////////////////////////////////////////////////////////////////////////////

/// Startup configuration parsed from arguments
typedef struct el_conf {
    struct in_addr bcast_address;
    long client_id;
} el_conf_t;

/// Parses the broadcast address and dumps it into `out`. Prints any errors.
/// Returns zero on success, `EXIT_ARGS` otherwise.
int el_parse_arg_bcast_address(const char* in, struct in_addr* out);

/// Parses the client id and dumps it into `out`. Prints any errors.
/// Returns zero on success, `EXIT_ARGS` otherwise.
int el_parse_arg_client_id(const char* in, long* out);

/// Gets the current timespec advanced by `reltime` in milliseconds for use with a timedwait and pselect.
struct timespec get_waituntil(long long reltime);

#endif // LIBELECTION_H

