#!/bin/bash
set -e 
cd $(dirname "${BASH_SOURCE[0]}")

# Config
MAIN_EXE_FILE="election"
CHECKER_EXE_FILE="elcheck"
SETTER_EXE_FILE="elset"
IMAGE_TAG="election"

rm --force "election.sock"

# Build main executable
rm --force "$MAIN_EXE_FILE"
gcc \
    -static \
    -pthread \
    -O3 \
    -Wall \
    \
    election.c electionlib.c \
    \
    -o "$MAIN_EXE_FILE" 

# Build checker executable
rm --force "$CHECKER_EXE_FILE"
gcc \
    -static \
    -O3 \
    -Wall \
    \
    elcheck.c electionlib.c \
    \
    -o "$CHECKER_EXE_FILE"

# Build setter executable
rm --force "$SETTER_EXE_FILE"
gcc \
    -static \
    -O3 \
    -Wall \
    \
    elset.c electionlib.c \
    \
    -o "$SETTER_EXE_FILE"

# Build Docker image
docker build . -t "$IMAGE_TAG" 

