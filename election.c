#include <arpa/inet.h>
#include <errno.h>
#include <limits.h>
#include <pthread.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include "electionlib.h"


// CONFIGURATION ///////////////////////////////////////////////////////////////////////////////////////////////////////

#define SINGLE_TERM false


// CONSTANTS ///////////////////////////////////////////////////////////////////////////////////////////////////////////

#define PING_DELAY_MS 100
#define INACTIVITY_PERIOD_MS 1000

#define TERM_DELAY_MS 5000
#define TERM_DELAY_STAGGER 2000
#define TERM_RECHECK_MS 250
#define TERM_RECHECK_STAGGER 500

#define HASHTABLE_SIZE 521
#define COLLISION_SLOTS 8


// STRUCTS /////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// Peer color
typedef enum el_color {
    EL_COLOR_NONE   = 0,
    EL_COLOR_GREEN  = 1,
    EL_COLOR_RED    = 2
} el_color_t;

/// An entry describing a discovered peer
typedef struct el_peer {
    bool active;            ///< Whether we are currently receiving pings from this peer
    long client_id;         ///< Peer's identifier
    el_color_t color;       ///< Peer's color
    el_color_t repcolor;    ///< Peer's reported color
    struct in_addr addr;    ///< Peer's current address
    long long last_ping;    ///< Millis of the monotonic clock on which something has been last received from this peer
} el_peer_t;


/// A hash entry in `el_peer_map_t`
typedef struct el_peer_map_entry el_peer_map_entry_t;
struct el_peer_map_entry {
    size_t occupied_slots;              ///< Number of occupied slots
    el_peer_t slots[COLLISION_SLOTS];   ///< Same-hash slots
    el_peer_map_entry_t* next;          ///< Next set of same-hash slots
};

/// Hash map of peers (key is `client_id`)
typedef struct el_peer_map {
    size_t num_peers;
    size_t num_active_peers;
    el_peer_map_entry_t hashtable[HASHTABLE_SIZE];
} el_peer_map_t;

/// Iterator over an `el_peer_map_t`. May be initialized using `{ .map = &my_map }`
typedef struct el_peer_map_it {
    el_peer_map_t* map;
    size_t hashtable_index;
    el_peer_map_entry_t* entry;
    size_t entry_index;
} el_peer_map_it_t;


/// Main program state (shared among its threads)
typedef struct el_state {
    pthread_mutex_t mutex;           ///< Main state mutex (to protect state data and interruptible waits)
    pthread_cond_t cond;             ///< Main cond for interruptible waits
    long client_id;                  ///< My ID
    long leader_id;                  ///< The client ID of the current leader
    el_color_t color;                ///< The current color of this peer
    struct sockaddr_in saddr_bcast;  ///< Broadcast address
    int sockid;                      ///< The ID of the main UDP socket
    struct sockaddr_un saddr_chk;    ///< Health check socket address
    int sockid_chk;                  ///< The ID of the health check socket
    el_peer_map_t peers;             ///< Map of known peers

    _Atomic(bool) pinging;           ///< A flag signifying whether the program should send its `HELLO` messages
    _Atomic(bool) running;           ///< A flag signifying whether the program should continue running
} el_state_t;

/// Global state
el_state_t state = {};

// PEER MAP FUNCTIONS //////////////////////////////////////////////////////////////////////////////////////////////////

/// Gets a peer from `map`, with the specified `client_id`. If such a peer does not exist yet, it is created in
/// the `map`.
static el_peer_t* el_peer_map_get(el_peer_map_t* map, long client_id) {
    el_peer_map_entry_t* entry = &map->hashtable[client_id % HASHTABLE_SIZE];

search_entry:
    for (size_t i = 0; i < entry->occupied_slots; i++) {
        if (entry->slots[i].client_id == client_id)  return &entry->slots[i];
    }

    if (entry->occupied_slots >= COLLISION_SLOTS) {
        if (entry->next) {
            entry = entry->next;
            goto search_entry;
        } else {
            entry->next = malloc(sizeof(el_peer_map_entry_t));
            entry = entry->next;
        }
    }

    size_t index = entry->occupied_slots;
    entry->occupied_slots++;
    map->num_peers++;

    entry->slots[index].client_id = client_id;
    return &entry->slots[index];
}

/// Frees the heap-allocated parts of the specified map.
static void el_peer_map_free(el_peer_map_t* map) {
    for (size_t i = 0; i < HASHTABLE_SIZE; i++) {
        el_peer_map_entry_t* entry = map->hashtable[i].next;
        while (entry) {
            el_peer_map_entry_t* freed = entry;
            entry = entry->next;
            free(freed);
        }
    }
}

/// Gets the next valid peer and advances the specified iterator. May return `NULL` if there is nothing left in the map.
static el_peer_t* el_peer_map_next(el_peer_map_it_t* it) {
determine_next_entry:
    if (it->hashtable_index >= HASHTABLE_SIZE)  return NULL;
    if (!it->entry)  it->entry = &it->map->hashtable[it->hashtable_index];

search_entry:
    if (it->entry_index >= it->entry->occupied_slots) {
        // There are no more occupied slots in this entry
        it->entry_index = 0;
        if (it->entry->next) {
            // There is another entry here, advance to it and jump back to search it
            it->entry = it->entry->next;
            goto search_entry;
        } else {
            // There are no more entries here, advance to the next hashtable index and jump to the beginning
            it->entry = NULL;
            it->hashtable_index++;
            goto determine_next_entry;
        }
    }

    // Advance the iterator and return the current slot
    return &it->entry->slots[it->entry_index++];
}

// UTILITIES ///////////////////////////////////////////////////////////////////////////////////////////////////////////

static int parse_args(int argc, char** argv, el_conf_t* out) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <bcast_address> <client_id>\n", argv[0]);
        return EXIT_ARGS;
    }

    int retval = 0;
    if (el_parse_arg_bcast_address(argv[1], &out->bcast_address))
        retval = EXIT_ARGS;
    if (el_parse_arg_client_id(argv[2], &out->client_id))
        retval = EXIT_ARGS;
    return retval;
}

static inline long long msof(struct timespec ts) {
    return ((long long) ts.tv_sec * 1000) + ((long long) ts.tv_nsec / 1000000);
}

/// Gets the current time of `CLOCK_MONOTONIC` in milliseconds.
static long long current_ms() {
    struct timespec ts;
    if (clock_gettime(CLOCK_MONOTONIC, &ts)) {
        perror("clock_gettime");
        return -1;
    }

    return msof(ts);
}

/// Waits for the specified amount of milliseconds. `state->mutex` must be locked for this to work properly!
static int guaranteed_wait(el_state_t* state, long long reltime) {
    struct timespec waituntil = get_waituntil(reltime);
    long long end = msof(waituntil);

    while (state->running && current_ms() <= end) {
        errno = pthread_cond_timedwait(&state->cond, &state->mutex, &waituntil);
        if (errno && errno != ETIMEDOUT) {
            return errno;
        }
    }

    errno = 0;
    return 0;
}

static inline const char* el_color_str(el_color_t color) {
    switch (color) {
        case EL_COLOR_GREEN:  return "GREEN";
        case EL_COLOR_RED:    return "RED";
        default:              return "NONE";
    }
}


// MAIN PEER LOGIC /////////////////////////////////////////////////////////////////////////////////////////////////////

/// Signal handler
static void sig_handler(int signum) {
    if (signum == SIGINT)
        puts("SIGINT received");
    if (signum == SIGTERM)
        puts("SIGTERM received");

    state.running = false;
    pthread_cond_broadcast(&state.cond);
}

/// The ping processing loop
static void* thrproc_pinger(void* state_ptr) {
    el_state_t* state = state_ptr;

    el_msg_color_t hello_pck = {
        .header = {
            .magic = PROTOCOL_MAGIC,
            .client_id = htonl(state->client_id),
            .type = htons(EL_MSG_HELLO),
            .length = htons(sizeof(hello_pck))
        },
        .color = state->color
    };

    while (state->running) {
        fflush(stdout);
        fflush(stderr);

        pthread_mutex_lock(&state->mutex);
        if (state->pinging) {
            hello_pck.color = state->color;

            // Send to peers
            ssize_t sent = sendto(state->sockid, &hello_pck, sizeof(hello_pck), 0,
                    (struct sockaddr*) &state->saddr_bcast, sizeof(state->saddr_bcast));

            if (sent < 0) {
                perror("sendto failed");
                pthread_mutex_unlock(&state->mutex);
                continue;
            }

            if (sent != sizeof(hello_pck)) {
                fputs("Could not send whole message\n", stderr);
                pthread_mutex_unlock(&state->mutex);
                continue;
            }

            // Send to healthcheck
            sent = sendto(state->sockid_chk, &hello_pck, sizeof(hello_pck), 0,
                    (struct sockaddr*) &state->saddr_chk, sizeof(state->saddr_chk));
            if (errno != ENOENT && errno != ECONNREFUSED) {
                if (sent < 0) {
                    perror("sendto healthcheck failed");
                    pthread_mutex_unlock(&state->mutex);
                } else if (sent != sizeof(hello_pck)) {
                    fputs("Could not send whole message to healthcheck\n", stderr);
                    pthread_mutex_unlock(&state->mutex);
                    continue;
                }
            }
        }

        // Check peer activity
        long long curr = current_ms();
        el_peer_map_it_t it = { .map = &state->peers };
        el_peer_t* peer;
        while ((peer = el_peer_map_next(&it))) {
            if (!peer->active)  continue;
            if ((curr - peer->last_ping) > INACTIVITY_PERIOD_MS) {
                printf("Peer #%ld lost :-(\n", peer->client_id);
                state->peers.num_active_peers--;
                peer->active = false;
            }
        }

        guaranteed_wait(state, PING_DELAY_MS);
        pthread_mutex_unlock(&state->mutex);
    }

    fflush(stdout);
    fflush(stderr);

    return NULL;
}

static void* thrproc_election(void* state_ptr) {
    el_state_t* state = state_ptr;
    if (!state->running)  return NULL;

    int term = 0;

term_begin:
    pthread_mutex_lock(&state->mutex);

    term++;
    state->leader_id = 0;


    {   // Reset colors
        el_peer_map_it_t it = { .map = &state->peers };
        el_peer_t* peer;
        while ((peer = el_peer_map_next(&it))) {
            peer->color = EL_COLOR_NONE;
        }
    }

    printf("Pre-term %d...\n", term);
    if (guaranteed_wait(state, TERM_DELAY_MS + (rand() % TERM_DELAY_STAGGER))) {
        perror("wait");
        goto exit_unlock;
    }
    if (!state->running)  goto exit_unlock;

    printf("==== Term %d ====\n", term);
    if (state->peers.num_active_peers == 0) {
        puts("No candidates found for this term, advancing to the next term... (strange, there should be at least me)");
        pthread_mutex_unlock(&state->mutex);
        goto term_begin;
    }

    size_t last_num_peers = state->peers.num_peers;
    size_t last_num_active_peers = state->peers.num_active_peers;

    // Find the leader among all currently discovered active peers (by the lowest ID)
    long leader_id = LONG_MAX;
    {
        el_peer_map_it_t it = { .map = &state->peers };
        el_peer_t* peer;
        while ((peer = el_peer_map_next(&it))) {
            if (!peer->active)  continue;
            if (peer->client_id < leader_id)  leader_id = peer->client_id;
        }
    }

    state->leader_id = leader_id;
    bool am_leader = (leader_id == state->client_id);

    // Determine colors internally
    if (am_leader) {
        puts("I am the leader");
        size_t max_greens = (state->peers.num_active_peers / 3) - 1; // Decremented because self state is special
        state->color = EL_COLOR_GREEN;

        {
            el_peer_map_it_t it = { .map = &state->peers };
            el_peer_t* peer;

            size_t colored = 0;
            while ((peer = el_peer_map_next(&it))) {
                if (!peer->active)  continue;
                if (peer->client_id == state->client_id) {
                    peer->color = EL_COLOR_GREEN;
                    continue;
                }

                if (colored < max_greens)
                    peer->color = EL_COLOR_GREEN;
                else
                    peer->color = EL_COLOR_RED;

                colored++;
            }
        }
    } else {
        printf("Peer #%ld is the leader\n", leader_id);
    }

    if (!state->running)  goto exit_unlock;


    while (last_num_peers == state->peers.num_peers && last_num_active_peers == state->peers.num_active_peers) {
        // Send out colors
        if (am_leader) {
            el_msg_color_t pck = {
                .header = {
                    .magic = PROTOCOL_MAGIC,
                    .client_id = htonl(state->client_id),
                    .type = htons(EL_MSG_SETCOLOR),
                    .length = htons(sizeof(pck))
                }
            };

            struct sockaddr_in saddr_out = {
                .sin_family = AF_INET,
                .sin_port = htons(EL_PORT)
            };

            {
                el_peer_map_it_t it = { .map = &state->peers };
                el_peer_t* peer;
                while ((peer = el_peer_map_next(&it))) {
                    if (!peer->active || peer->client_id == state->client_id)  continue;

                    pck.color = peer->color;
                    saddr_out.sin_addr = peer->addr;
                    ssize_t sent = sendto(state->sockid, &pck, sizeof(pck), 0,
                                          (struct sockaddr*) &saddr_out, sizeof(saddr_out));
                    if (sent < 0) {
                        perror("sendto failed");
                        continue;
                    }
                    if (sent != sizeof(pck)) {
                        fputs("Could not send whole message\n", stderr);
                        continue;
                    }
                }
            }
        }

        // Wait for recheck (unlocks the mutex temporarily)
        if (guaranteed_wait(state, TERM_RECHECK_MS + (rand() % TERM_RECHECK_STAGGER))) {
            perror("wait");
            goto exit_unlock;
        }
        if (!state->running)  goto exit_unlock;

        {
            size_t nones = 0;
            size_t greens = 0;
            size_t reds = 0;

            el_peer_map_it_t it = { .map = &state->peers };
            el_peer_t* peer;
            while ((peer = el_peer_map_next(&it))) {
                if (!peer->active)  continue;

                if (peer->repcolor == EL_COLOR_NONE)  nones++;
                else if (peer->repcolor == EL_COLOR_GREEN)  greens++;
                else if (peer->repcolor == EL_COLOR_RED)  reds++;
            }

            printf("Current state:\n"
                   " - My color:   %s\n"
                   " - My ID:      %ld\n"
                   " - Leader ID:  %ld\n"
                   " - %zd nones, %zd greens, %zd reds\n",
                   el_color_str(state->color), state->client_id, state->leader_id, nones, greens, reds);

            if (SINGLE_TERM && am_leader && nones == 0) {
                puts("There are no more peers with color NONE, broadcasting a STOP signal ...");

                state->running = false;
                pthread_cond_broadcast(&state->cond);

                el_msg_t stop_pck = {
                    .magic = PROTOCOL_MAGIC,
                    .client_id = htonl(state->client_id),
                    .type = htons(EL_MSG_STOP),
                    .length = htons(sizeof(stop_pck))
                };

                ssize_t sent = sendto(state->sockid, &stop_pck, sizeof(stop_pck), 0,
                                      (struct sockaddr*) &state->saddr_bcast, sizeof(state->saddr_bcast));
                if (sent < 0) {
                    perror("sendto failed");
                    goto exit_unlock;
                }
                if (sent != sizeof(stop_pck)) {
                    fputs("Could not send whole message\n", stderr);
                    goto exit_unlock;
                }

                goto exit_unlock;
            }
        }
    }
    pthread_mutex_unlock(&state->mutex);
    goto term_begin;

exit_unlock:
    pthread_mutex_unlock(&state->mutex);
    return NULL;
}

/// Handle a received message. The mutex of `state` needs to be locked.
static void handle_msg(el_state_t* state, el_peer_t* peer, el_msg_t* msg) {
    if (peer) {
        peer->last_ping = current_ms();
        if (!peer->active) {
            state->peers.num_active_peers++;
            peer->active = true;

            printf("Discovered peer #%ld on address %s", peer->client_id, inet_ntoa(peer->addr));
            if (peer->client_id == state->client_id)  fputs(" (^_^ that's me!)", stdout);
            putchar('\n');

            fflush(stdout);
        }
    }

    bool am_leader = (state->leader_id == state->client_id);
    uint16_t msg_type = ntohs(msg->type);
    switch (msg_type) {
        case EL_MSG_HELLO: {
            if (!peer) {
                fputs("A non-peer entity sent a HELLO message, ignoring", stderr);
                return;
            }

            el_color_t color = ((el_msg_color_t*) msg)->color;
            if (color != peer->color) {
                if (am_leader) {
                    printf("Peer #%ld reports color %s when it should be %s\n",
                           peer->client_id, el_color_str(color), el_color_str(peer->color));
                }
            }

            if (color != peer->repcolor) {
                if (!am_leader)  peer->color = color;
                peer->repcolor = color;
                printf("Peer #%ld reports color %s\n", peer->client_id, el_color_str(color));
            }
        }
        break;

        case EL_MSG_SETCOLOR: {
            if (!peer) {
                puts("A non-peer entity sent a SETCOLOR message, ignoring");
                return;
            }

            if (peer->client_id != state->leader_id) {
                printf("Peer #%ld wants to set my color, but they're not the leader!\n", peer->client_id);
                return;
            }

            el_color_t color = ((el_msg_color_t*) msg)->color;
            if (color != state->color) {
                state->color = color;
                printf("Leader set my color to %s\n", el_color_str(state->color));
            }
        }
        break;

        case EL_MSG_STOP: {
            if (peer)
                printf("Peer #%ld has issued a stop - quitting gracefully ...\n", peer->client_id);
            else
                puts("Non-peer entity has issued a stop - quitting gracefully ...\n");

            state->running = false;
            pthread_cond_broadcast(&state->cond);
        }
        break;

        case EL_MSG_SET_PING: {
            el_msg_set_ping_t* pmsg = (el_msg_set_ping_t*) msg;
            state->pinging = pmsg->enable != 0;
            printf("Pinging has been set to %s\n", state->pinging ? "ENABLED" : "DISABLED");
        }
        break;

        default: {
            printf("Unknown message type %d", msg->type);
            if (peer)
                printf(" from peer #%ld", peer->client_id);
            putchar('\n');
        }
    }
}

/// Main receiver logic
static void receiver_logic_proc(el_state_t* state) {
    char msg_buffer[512];
    el_msg_t* msg = (el_msg_t*) msg_buffer;
    fd_set fds;

    while (state->running) {
        fflush(stderr);
        fflush(stdout);

        // Recv timeout
        FD_ZERO(&fds);
        FD_SET(state->sockid, &fds);
        struct timespec ts = { .tv_nsec = 100 * 1000000 };
        int sel = pselect(state->sockid + 1, &fds, NULL, NULL, &ts, NULL);
        if (sel < 0) {
            perror("pselect");
            continue;
        }
        if (!FD_ISSET(state->sockid, &fds))
            continue;

        char addr_buffer[128];
        struct sockaddr* addr = (struct sockaddr*) addr_buffer;
        *((struct sockaddr_in*) addr) = state->saddr_bcast;
        socklen_t addr_size = sizeof(addr_buffer);

        ssize_t received = recvfrom(state->sockid, msg_buffer, sizeof(msg_buffer), 0, addr, &addr_size);

        if (received < 0) {
            perror("recvfrom failed");
            state->running = false;
            break;
        }

        if (received != ntohs(msg->length)) {
            fputs("Incorrect received message size\n", stderr);
            continue;
        }

        if (msg->magic != PROTOCOL_MAGIC) {
            fputs("WTF!\n", stderr);
            continue;
        }

        struct in_addr inaddr = {};
        if (addr_size == sizeof(struct sockaddr_in)) {
            inaddr = ((struct sockaddr_in*) addr)->sin_addr;
        }

        uint32_t sender_id = ntohl(msg->client_id);
        pthread_mutex_lock(&state->mutex);
        {
            el_peer_t* peer;
            if (sender_id) {
                peer = el_peer_map_get(&state->peers, sender_id);
                peer->addr = inaddr;
            } else {
                peer = NULL;
            }

            handle_msg(state, peer, msg);
        }
        pthread_mutex_unlock(&state->mutex);
    }

    fflush(stderr);
    fflush(stdout);
}


// ENTRY POINT /////////////////////////////////////////////////////////////////////////////////////////////////////////

/// Program entry point - prepares config, socket, pinger thread; then calls the main peer logic.
int main(int argc, char** argv) {
    const int ENABLE = 1;

    signal(SIGINT, sig_handler);

    el_conf_t conf;
    if (parse_args(argc, argv, &conf)) {
        return EXIT_ARGS;
    }

    srand(current_ms());

    printf("Broadcast address:  %s\n"
           "My ID:              %lu\n",
           inet_ntoa(conf.bcast_address), conf.client_id);
    fflush(stdout);

    int retval = 0;
    state.client_id = conf.client_id;
    state.color = EL_COLOR_NONE;
    state.pinging = true;
    state.running = true;

    // _mutex
    if ((errno = pthread_mutex_init(&state.mutex, NULL))) {
        perror("Could not initialize mutex");
        return EXIT_SYS;
    }

    // _cond
    pthread_condattr_t condattr;
    pthread_condattr_init(&condattr);
    pthread_condattr_setclock(&condattr, CLOCK_MONOTONIC);
    if ((errno = pthread_cond_init(&state.cond, &condattr))) {
        pthread_condattr_destroy(&condattr);
        perror("Could not initialize cond");
        retval = EXIT_SYS;
        goto exit_mutex;
    }
    pthread_condattr_destroy(&condattr);

    // _sock
    if ((state.sockid = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
        perror("Could not create main UDP socket");
        retval = EXIT_SYS;
        goto exit_cond;
    }

    if (setsockopt(state.sockid, SOL_SOCKET, SO_BROADCAST, &ENABLE, sizeof(ENABLE))) {
        perror("Could not set broadcast permission for main UDP socket");
        retval = EXIT_SYS;
        goto exit_sock;
    }

    struct sockaddr_in saddr_bound = {
        .sin_family = AF_INET,
        .sin_port = htons(EL_PORT),
        .sin_addr = {
            .s_addr = htonl(INADDR_ANY)
        }
    };

    if (bind(state.sockid, (struct sockaddr*) &saddr_bound, sizeof(saddr_bound)) < 0) {
        perror("Failed to bind main UDP socket");
        retval = EXIT_SYS;
        goto exit_sock;
    }

    state.saddr_bcast.sin_family = AF_INET;
    state.saddr_bcast.sin_port = htons(EL_PORT);
    state.saddr_bcast.sin_addr = conf.bcast_address;

    // _sock_chk
    if ((state.sockid_chk = socket(AF_UNIX, SOCK_DGRAM, 0)) < 0) {
        perror("Could not create healthcheck socket");
        retval = EXIT_SYS;
        goto exit_sock;
    }

    state.saddr_chk.sun_family = AF_UNIX;
    strncpy(state.saddr_chk.sun_path, EL_SOCK_CHK_NAME, sizeof(state.saddr_chk.sun_path));

    // _pinger
    pthread_t pinger_thread;
    if ((errno = pthread_create(&pinger_thread, NULL, thrproc_pinger, &state))) {
        perror("Could not create pinger thread");
        state.running = false;
        retval = EXIT_SYS;
        goto exit_sock_chk;
    }

    // _election
    pthread_t election_thread;
    if ((errno = pthread_create(&election_thread, NULL, thrproc_election, &state))) {
        perror("Could not create election thread");
        state.running = false;
        retval = EXIT_SYS;
        goto exit_pinger;
    }

    receiver_logic_proc(&state);

    puts("Cave Johnson. We're done here.");

    pthread_join(election_thread, NULL);
exit_pinger:
    pthread_join(pinger_thread, NULL);
    el_peer_map_free(&state.peers);
exit_sock_chk:
    close(state.sockid_chk);
exit_sock:
    close(state.sockid);
exit_cond:
    pthread_cond_destroy(&state.cond);
exit_mutex:
    pthread_mutex_destroy(&state.mutex);
    return retval;
}

