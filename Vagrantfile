#
# *** Demo 3
# Create N client nodes and 1 broker node working as a load-balancer.
#

VAGRANTFILE_API_VERSION = "2"
# set docker as the default provider
ENV['VAGRANT_DEFAULT_PROVIDER'] = 'docker'
# disable parallellism so that the containers come up in order
#ENV['VAGRANT_NO_PARALLEL'] = "1"
#ENV['FORWARD_DOCKER_PORTS'] = "1"
# minor hack enabling to run the image and configuration trigger just once
ENV['VAGRANT_EXPERIMENTAL']="typed_triggers"

unless Vagrant.has_plugin?("vagrant-docker-compose")
  system("vagrant plugin install vagrant-docker-compose")
  puts "Dependencies installed, please try the command again."
  exit
end

# Names of Docker images built:
IMAGE_TAG = "election:latest"

# Node definitions
CLIENTS  = { 
  :nameprefix => "el-client-",  # client nodes get names: el-client-1, el-client-2, etc.
  :subnet => "10.0.36.",
  :ip_offset => 100,  # client nodes get IP addresses: 10.0.1.101, .102, .103, etc
  :image => IMAGE_TAG,
  :port => 1337
}

# Number of CLIENTS to start:
CLIENTS_COUNT = 12

# Common configuration
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  # Before the 'vagrant up' command is started, build docker images:
  config.trigger.before :up, type: :command do |trigger|
    trigger.name = "Build docker images and configuration files"
    trigger.ruby do |env, machine|
      # --- start of Ruby script ---

      puts "Building everything:"
      `./build.sh`

      # --- end of Ruby script ---
    end
  end

  #config.vm.synced_folder ".", "/vagrant", type: "rsync", rsync__exclude: ".*/"
  #config.ssh.insert_key = false
  
  last_id = 1
  ids = Array.new(CLIENTS_COUNT)
  rand = Random.new

  # Generation of random IDs
  (0...CLIENTS_COUNT).each do |i|
    last_id += rand.rand(101)
    ids[i] = last_id
  end

  ids.shuffle!

  # Definition of N CLIENTS
  (1..CLIENTS_COUNT).each do |i|
    node_ip_addr = "#{CLIENTS[:subnet]}#{CLIENTS[:ip_offset] + i}"
    node_name = "#{CLIENTS[:nameprefix]}#{i}"
    # Definition of CLIENT
    config.vm.define node_name do |s|
      s.vm.network "private_network", ip: node_ip_addr, protocol: "udp"
      s.vm.hostname = node_name
      s.vm.provider "docker" do |d|
        d.image = CLIENTS[:image]
        d.name = node_name
        d.env = { 
          "BCAST_ADDRESS" => "#{CLIENTS[:subnet]}255",
          "NUM_CLIENTS" => "#{CLIENTS_COUNT}",
          "CLIENT_ID" => "#{ids[i - 1]}"
        }
        #d.has_ssh = true
      end
      #s.vm.post_up_message = "Node #{node_name} up and running. You can access the node with 'vagrant ssh #{node_name}'}"
    end
  end

end

