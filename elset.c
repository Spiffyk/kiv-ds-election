#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include "electionlib.h"

typedef enum {
    EL_ACTION_PING = 0x01,
    EL_ACTION_NOPING = 0x02
} action_t;

typedef struct conf {
    struct in_addr address;
    action_t action;
} conf_t;


static int parse_action(const char* in, action_t* out) {
    if (strcmp(in, "ping") == 0) {
        *out = EL_ACTION_PING;
        return EXIT_SUCCESS;
    } else if (strcmp(in, "noping") == 0) {
        *out = EL_ACTION_NOPING;
        return EXIT_SUCCESS;
    }

    fprintf(stderr, "Unknown action '%s' - known: ping, noping\n", in);
    return EXIT_ARGS;
}

static int parse_args(int argc, char** argv, conf_t* out) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <peer_address> <action>\n", argv[0]);
        return EXIT_ARGS;
    }

    int retval = 0;

    if (el_parse_arg_bcast_address(argv[1], &out->address))
        retval = EXIT_ARGS;
    if (parse_action(argv[2], &out->action))
        retval = EXIT_ARGS;

    return retval;
}

// ENTRY POINT /////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv) {
    const int ENABLE = 1;

    conf_t conf;
    if (parse_args(argc, argv, &conf))
        return EXIT_ARGS;

    int sockid;
    if ((sockid = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
        perror("socket");
        return EXIT_SYS;
    }

    int retval = EXIT_SUCCESS;

    if (setsockopt(sockid, SOL_SOCKET, SO_BROADCAST, &ENABLE, sizeof(ENABLE))) {
        perror("Could not set broadcast permission");
        retval = EXIT_SYS;
        goto exit_sock;
    }

    struct sockaddr_in saddr = {
        .sin_family = AF_INET,
        .sin_port = htons(EL_PORT),
        .sin_addr = conf.address
    };

    el_msg_set_ping_t msg = {
        .header = {
            .magic = PROTOCOL_MAGIC,
            .client_id = 0,
            .type = htons(EL_MSG_SET_PING),
            .length = htons(sizeof(msg))
        },
        .enable = (conf.action == EL_ACTION_PING)
    };

    ssize_t sent = sendto(sockid, &msg, sizeof(msg), 0,
            (struct sockaddr*) &saddr, sizeof(saddr));

    if (sent < 0) {
        perror("sendto failed");
        retval = EXIT_SYS;
        goto exit_sock;
    }

    if (sent != sizeof(msg)) {
        fputs("Could not send whole message\n", stderr);
        retval = EXIT_SYS;
        goto exit_sock;
    }

exit_sock:
    close(sockid);
    return retval;
}

