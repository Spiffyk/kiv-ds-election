# KIV/DS - Výběr leadera 2

**Autor:** Bc. Oto Šťáva (A20N0107P)

Tato práce je implementována v jazyce C a využívá UDP broadcasty ve virtuální síti služby Docker. Komunikační náročnost implementovaného algoritmu je díky broadcastům *O(1)* s výjimkou chvíle, kdy *leader* posílá barvy jednotlivým *followerům*: tehdy je komunikační náročnost *O(n)*.


## Generování kontejnerů

Soubor `Vagrantfile` vytvoří *n* kontejnerů s totožným programem `election`. Jako argumenty tomuto programu předá broadcast adresu virtuální sítě a jedinečný náhodný identifikátor instance. Na základě tohoto identifikátoru se volí leader.


## Běh programu

Každá instance poslouchá na portu 1337 a zároveň na něm v pravidelných intervalech vysílá svou `HELLO` zprávu, ve které je rovněž obsažen příznak s informací, kterou barvu aktuálně tato instance má (z možností `NONE`, `GREEN`, `RED`).

Přijímáním zpráv `HELLO` se o sobě všechny instance postupně vzájemně "dozvědí" a po uplynulém konstantním čase zvolí *leadera* - tím je instance s nejnižším identifikátorem. Leader pak každé instanci včetně sebe přiřadí barvu zprávou `SETCOLOR` tak, aby 1/3 z nich měla barvu `GREEN` a zbylé 2/3 měly barvu `RED`. O změnách barev se dozvídají všechny instance z následujících `HELLO` zpráv.

Program standardně počítá s výpadky klientů (včetně leadera). Jakmile dojde k jakékoliv změně ve stavech klientů, začíná nové *volební období* a volí se nový *leader*, který opět přiřadí nové barvy ostatním běžícím instancím programu.


## Kontrola zdraví kontejneru

V projektu se nacházejí rovněž programy `elcheck` a `elset`, které jsou instrumentální ke *kontrole zdraví kontejneru* s programem `election` a k testování této kontroly.

### elcheck

Program `elcheck` vytvoří v pracovním adresáři Unixový socket `election.sock`, ze kterého začne přijímat datagramy. Program `election` do tohoto socketu okamžitě po jeho vzniku začne zapisovat kopie zprávy `HELLO`, kterou posílá na broadcast adresu. Jakmile program `elcheck` přijme několik těchto zpráv `HELLO`, prohlásí kontejner za *zdravý*. Pokud dlouho zpráva nechodí, či pokud je v nesprávném formátu, je kontejner prohlášen za *nezdravý*. Soubor `Dockerfile` je konfigurován tak, že je program `elcheck` spuštěn jednou za 5 sekund.


### elset

Program `elset` slouží k demonstraci *kontroly zdraví kontejneru* a ztráty spojení klientů a je určen ke spouštění na hostitelském počítači (nikoliv uvnitř kontejneru). Lze jím na jednotlivých instancích programu `election` posílat zprávy `SET_PING` pro vypínání a zapínání vysílání zprávy `HELLO`.

Spouští se následujícím příkazem:

```
$ ./elset <adresa_kontejneru> <akce>
```

Hodnota `adresa_kontejneru` je standardně `10.0.36.(číslo klienta + 100)`, popř. `10.0.36.255` jako broadcast adresa pro odeslání zprávy všem klientům.

Hodnota `akce` může být `ping` či `noping` - lze tak zapnout či vypnout vysílání zprávy `HELLO`.

Tedy například:

```
$ ./elset 10.0.36.105 noping
```

vypne vysílání zprávy `HELLO` pro klienta č. 5.


## Sestavení a spuštění

Programy lze sestavit a spustit nástrojem Vagrant:

```
$ vagrant up
```

Tento příkaz sestaví jednotlivé programy a obraz kontejneru pomocí skriptu `build.sh` a spustí *n* jeho instancí.

